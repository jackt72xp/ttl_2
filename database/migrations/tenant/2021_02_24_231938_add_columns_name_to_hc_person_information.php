<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsNameToHcPersonInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hc_person_information', function (Blueprint $table) {
            $table->string('last_name')->after('person_id')->nullable();
            $table->string('name')->after('person_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hc_person_information', function (Blueprint $table) {
            $table->dropColumn(['name', 'last_name']);
        });
    }
}
