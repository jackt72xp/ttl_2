<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcAnamnesesPathologies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_anamneses_pathologies', function (Blueprint $table) {
            $table->unsignedInteger('anamnesis_id');
            $table->unsignedInteger('medical_pathology_id');
            $table->timestamps();

            $table->foreign('anamnesis_id')->references('id')->on('hc_anamneses');
            $table->foreign('medical_pathology_id')->references('id')->on('hc_cat_persistent_pathologies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_anamneses_pathologies');
    }
}
