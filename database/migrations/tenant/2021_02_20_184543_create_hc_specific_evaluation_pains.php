<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcSpecificEvaluationPains extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_specific_evaluation_pains', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('general_evaluation_id');
            $table->string('reason', 600);
            $table->string('description', 600)->nullable();
            $table->string('how_long');
            $table->string('how_many_times');
            $table->string('related')->nullable();
            $table->string('specific_injury')->nullable();
            $table->string('type_pain_niusance');
            $table->string('evolution_pain_day', 100);
            $table->string('aggravates_condition');
            $table->string('makes_you_better');
            $table->string('functional_limitation_avd')->nullable();
            $table->string('functional_limitation_work')->nullable();
            $table->string('functional_limitation_recreation')->nullable();
            $table->unsignedInteger('articular_id');
            $table->string('body_zone', 50);
            $table->char('status', 2);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('general_evaluation_id')->references('id')->on('hc_general_evaluations');
            $table->foreign('articular_id')->references('id')->on('hc_cat_articulations');
            $table->foreign('status')->references('id')->on('hc_status_evaluation_pains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_specific_evaluation_pains');
    }
}
