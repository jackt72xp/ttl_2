<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcGeneralEvaluationLimitations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_general_evaluation_limitations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('general_evaluation_id');
            $table->string('type', 20);
            $table->unsignedInteger('articular_id')->nullable();
            $table->unsignedInteger('muscle_id')->nullable();
            $table->string('result');
            $table->string('observations', 400)->nullable();
            $table->json('movement');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('general_evaluation_id')->references('id')->on('hc_general_evaluations');
            $table->foreign('articular_id')->references('id')->on('hc_cat_articulations');
            $table->foreign('muscle_id')->references('id')->on('hc_cat_muscles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_general_evaluation_limitations');
    }
}
