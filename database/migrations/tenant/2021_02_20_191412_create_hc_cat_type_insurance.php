<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcCatTypeInsurance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_cat_type_insurance', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('hc_cat_type_insurance')->insert([

            ['name' => 'EPS', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'ESSALUD', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'SIS', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_cat_type_insurance');
    }
}
