<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcHcGeneralEvaluationTests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_hc_general_evaluation_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('general_evaluation_id');
            $table->unsignedInteger('medical_test_id');
            $table->string('type', 20);
            $table->string('result');
            $table->boolean('positive')->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('general_evaluation_id')->references('id')->on('hc_general_evaluations');
            $table->foreign('medical_test_id')->references('id')->on('hc_cat_medical_test');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_hc_general_evaluation_tests');
    }
}
