<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcTreatmentPunctures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_treatment_punctures', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('treatment_id');
            $table->unsignedInteger('muscle_id');
            $table->json('muscle');
            $table->unsignedInteger('articulation_id');
            $table->json('articulation');
            $table->enum('spasm_left', ['leve', 'regular', 'intenso']);
            $table->enum('spasm_right', ['leve', 'regular', 'intenso']);
            $table->boolean('medium')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('treatment_id')->references('id')->on('hc_treatments');
            $table->foreign('muscle_id')->references('id')->on('hc_cat_muscles');
            $table->foreign('articulation_id')->references('id')->on('hc_cat_articulations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_treatment_punctures');
    }
}
