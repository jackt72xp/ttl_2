<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcTreatments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_treatments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('specific_evaluation_pain_id');
            $table->enum('type', ['tto1', 'ttoN']);
            $table->date('date');
            $table->string('physical_agents');
            $table->string('used_needles', 3);
            $table->string('how_pain_now');
            $table->string('observations', 300)->nullable();
            $table->string('doctor', 200)->nullable();
            $table->string('mode', 30)->nullable();
            $table->boolean('do_exercises')->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('specific_evaluation_pain_id')->references('id')->on('hc_specific_evaluation_pains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_treatments');
    }
}
