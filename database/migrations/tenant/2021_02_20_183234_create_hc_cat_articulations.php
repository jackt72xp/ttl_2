<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcCatArticulations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_cat_articulations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('hc_cat_articulations')->insert([
            ['name' => 'HOMBRO', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'CODO', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'MUÑECA', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'CERVICAL', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'DORSAL', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'LUMBAR', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'CADERA', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'RODILLA', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'TOBILLO', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'INTERFALÁNGICAS PIE', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'METACARPO FALÁNGICAS', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'MATATARSO FALÁNGICAS PROXIMAL', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'MATATARSO FALÁNGICAS DISTAL', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_cat_articulations');
    }
}
