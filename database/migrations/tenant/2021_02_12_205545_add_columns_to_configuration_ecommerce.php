<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToConfigurationEcommerce extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configuration_ecommerce', function (Blueprint $table) {
            $table->text('link_instagram')->nullable()->after('link_facebook');
            $table->string('color_navbar', 30)->nullable()->after('token_private_culqui');
            $table->string('color_footer', 30)->nullable()->after('token_private_culqui');
            $table->string('title_web', 50)->nullable()->after('token_private_culqui');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configuration_ecommerce', function (Blueprint $table) {
            $table->dropColumn(['link_instagram', 'color_navbar', 'color_footer', 'title_web']);
        });
    }
}
