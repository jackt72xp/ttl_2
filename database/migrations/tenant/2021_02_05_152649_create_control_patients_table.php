<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_patient', 20);
            $table->unsignedInteger('customer_id');
            $table->time('appointment_time');
            $table->date('appointment_date');
            $table->string('appointment_request', 20);
            $table->unsignedInteger('service_id')->nullable();
            $table->unsignedInteger('therapist_id')->nullable();
            $table->string('hours', 2)->nullable();
            $table->string('session_current', 2)->nullable();
            $table->string('session_numbe_total', 2)->nullable();
            $table->unsignedInteger('promotion_id')->nullable();
            $table->string('payment_type', 25)->nullable();
            $table->unsignedInteger('document_id')->nullable();

            $table->foreign('customer_id')->references('id')->on('persons');
            $table->foreign('service_id')->references('id')->on('service_consults');
            $table->foreign('therapist_id')->references('id')->on('therapists');
            $table->foreign('promotion_id')->references('id')->on('promotion_consults');
            $table->foreign('document_id')->references('id')->on('documents');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_patients');
    }
}
