<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnamnesesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_anamneses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('clinic_history_id');
            $table->boolean('has_previous_physiotherapy')->default(false);
            $table->string('name_previous_physiotherapy')->nullable();
            $table->date('date_previous_physiotherapy')->nullable();

            $table->boolean('has_chiropractor')->default(false);
            $table->date('has_chiropractor_date')->nullable();

            $table->boolean('has_osteopathy')->default(false);
            $table->date('has_osteopathy_date')->nullable();

            $table->boolean('has_acupuncture')->default(false);
            $table->date('has_acupuncture_date')->nullable();

            $table->string('has_other_description')->nullable();
            $table->date('has_other_date')->nullable();

            $table->boolean('has_psychological_diagnosis')->default(false);
            $table->string('psychological_diagnosis_description')->nullable();

            $table->boolean('has_surgery')->default(false);
            $table->date('surgery_date')->nullable();
            $table->string('surgery_description')->nullable();

            $table->boolean('has_fracture')->default(false);
            $table->date('fracture_date')->nullable();
            $table->string('fracture_description')->nullable();


            $table->boolean('has_cancer')->default(false);
            $table->date('cancer_date')->nullable();
            $table->boolean('cancer_family_direct')->default(false);
            $table->string('cancer_description')->nullable();

            $table->boolean('has_smoke')->default(false);
            $table->string('smoke_frecuency')->nullable();

            $table->boolean('has_allergy')->default(false);
            $table->string('allergy_description')->nullable();

            $table->boolean('has_physical_activity')->default(false);
            $table->string('physical_activity_frecuency')->nullable();
            $table->string('physical_activity_type')->nullable();


            $table->string('pathologie_general_other')->nullable();
            $table->string('pathologie_muscle_art_other')->nullable();
            $table->string('pathologie_gastro_other')->nullable();
            $table->string('pathologie_cardio_other')->nullable();
            $table->string('pathologie_infect_other')->nullable();
            $table->string('pathologie_respiratory_other')->nullable();
            $table->string('medications_other')->nullable();

            $table->timestamps();
            $table->softDeletes();

            //$table->foreign('user_id')->references('id')->on('users');
            $table->foreign('clinic_history_id')->references('id')->on('hc_clinic_histories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anamneses');
    }
}
