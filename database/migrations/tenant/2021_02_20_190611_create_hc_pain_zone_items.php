<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcPainZoneItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_pain_zone_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('body_segment');
            $table->string('body_direction');
            $table->string('eva', 2);
            $table->string('type_pain', 20);
            $table->unsignedInteger('pain_zone_id');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pain_zone_id')->references('id')->on('hc_pain_zones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_pain_zone_items');
    }
}
