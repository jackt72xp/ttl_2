<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcStatusEvaluationPains extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_status_evaluation_pains', function (Blueprint $table) {
            $table->char('id', 2)->index();
            $table->string('name');
            $table->string('description');
            $table->boolean('status')->default(true);
        });

        DB::table('hc_status_evaluation_pains')->insert([
            ['id' => '01', 'name' => 'available', 'description' => 'available'],
            ['id' => '02', 'name' => 'diagn-complete', 'description' => 'diagn-complete'],
            ['id' => '03', 'name' => 'ps-complete', 'description' => 'ps-complete'],
            ['id' => '04', 'name' => 'man-tera-complete', 'description' => 'man-tera-complete'],
            ['id' => '05', 'name' => 'exe-presc-complete', 'description' => 'exercise prescription'],
            ['id' => '06', 'name' => 'reeva-complete', 'description' => 'reeva-complete'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_status_evaluation_pains');
    }
}
