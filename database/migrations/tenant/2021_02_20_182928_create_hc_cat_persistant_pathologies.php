<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateHcCatPersistantPathologies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_cat_persistent_pathologies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('hc_cat_persistent_pathologies')->insert([

            ['type' => 'GENERAL','name' => 'Diabetes', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'GENERAL','name' => 'Hipotiroidismo', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'GENERAL','name' => 'Hipertiroidismo', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'GENERAL','name' => 'Pérdida de Visión', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'GENERAL','name' => 'Epilepsia', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'GENERAL','name' => 'Migraña', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'GENERAL','name' => 'Síncope / Desmayos', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],

            ['type' => 'MUSCULO_ARTICULACION','name' => 'Artritis', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'MUSCULO_ARTICULACION','name' => 'Osteoporosis', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'MUSCULO_ARTICULACION','name' => 'Rigidez', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'MUSCULO_ARTICULACION','name' => 'Debilidad Muscular', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'MUSCULO_ARTICULACION','name' => 'Derrame articular', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'MUSCULO_ARTICULACION','name' => 'Osteoartrosis', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],

            ['type' => 'GASTROINTESTINAL','name' => 'Gastritis', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'GASTROINTESTINAL','name' => 'Úlceras gástricas', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'GASTROINTESTINAL','name' => 'Intestino Irritable – diarrea', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'GASTROINTESTINAL','name' => 'Constipación', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],


            ['type' => 'CARDIOVASCULAR','name' => 'Hipertensión', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'CARDIOVASCULAR','name' => 'Hipotensión', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'CARDIOVASCULAR','name' => 'Edema de MMII', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'CARDIOVASCULAR','name' => 'Infarto cardiaco', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'CARDIOVASCULAR','name' => 'ACV', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'CARDIOVASCULAR','name' => 'Várices', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'CARDIOVASCULAR','name' => 'Trombosis', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'CARDIOVASCULAR','name' => 'Marcapaso', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],

            ['type' => 'INFECCIONES','name' => 'Hepatitis', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'INFECCIONES','name' => 'TBC', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'INFECCIONES','name' => 'VIH', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],

            ['type' => 'RESPIRATORIO','name' => 'Asma', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'RESPIRATORIO','name' => 'EPOC', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'RESPIRATORIO','name' => 'Fibrosis Pulmonar', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['type' => 'RESPIRATORIO','name' => 'Bronquitis Crónica', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_cat_persistent_pathologies');
    }
}
