<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcPainProgressions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_pain_progressions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('treatment_id');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('treatment_id')->references('id')->on('hc_treatments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_pain_progressions');
    }
}
