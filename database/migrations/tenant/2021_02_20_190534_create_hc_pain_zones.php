<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcPainZones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_pain_zones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pain_zone');
            $table->unsignedInteger('specific_evaluation_pain_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('specific_evaluation_pain_id')->references('id')->on('hc_specific_evaluation_pains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_pain_zones');
    }
}
