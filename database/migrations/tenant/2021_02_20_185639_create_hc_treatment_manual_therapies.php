<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcTreatmentManualTherapies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_treatment_manual_therapies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('treatment_id');
            $table->json('therapy');
            $table->string('description');
            $table->string('observations')->nullable();
            $table->string('type', 20);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('treatment_id')->references('id')->on('hc_treatments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_treatment_manual_therapies');
    }
}
