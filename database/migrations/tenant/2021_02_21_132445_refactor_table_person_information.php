<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorTablePersonInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('hc_person_information');

        Schema::create('hc_person_information', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('person_id')->nullable();
            $table->text('type_find_ttl')->nullable();
            $table->string('accompanied')->nullable();
            $table->string('medical_treating');
            $table->string('occupation');
            $table->string('observations')->nullable();
            $table->unsignedInteger('type_insurance_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('person_id')->references('id')->on('persons');
            $table->foreign('type_insurance_id')->references('id')->on('hc_cat_type_insurance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('hc_person_information', function (Blueprint $table) {
            $table->increments('id');
            $table->text('type_find_ttl')->nullable();
            $table->string('accompanied')->nullable();
            $table->string('medical_treating');
            $table->string('occupation');
            $table->string('observations')->nullable();
            $table->unsignedInteger('type_insurance_id')->nullable();

            $table->char('country_id', 2);
            $table->char('department_id', 2)->nullable();
            $table->char('province_id', 4)->nullable();
            $table->char('district_id', 6)->nullable();

            $table->timestamps();
            $table->softDeletes();


            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('district_id')->references('id')->on('districts');

            $table->foreign('type_insurance_id')->references('id')->on('hc_cat_type_insurance');
        });
    }
}
