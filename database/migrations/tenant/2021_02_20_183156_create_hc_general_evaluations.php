<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcGeneralEvaluations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_general_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('clinic_history_id');
            $table->string('antecedent_child', 600)->nullable();
            $table->string('pain_started_another_area', 600)->nullable();
            $table->boolean('has_biomechanical_help')->default(false);
            $table->string('biomechanical_help_type')->nullable();
            $table->boolean('has_orthotic')->default(false);
            $table->string('orthotic_type')->nullable();
            $table->string('recovery_objective', 600)->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clinic_history_id')->references('id')->on('hc_clinic_histories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_general_evaluations');
    }
}
