<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcDiagnosisPhysios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_diagnosis_physios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('specific_evaluation_pain_id');
            $table->unsignedInteger('medical_dx_physio_id');
            $table->string('forecast');
            $table->string('structure_bio')->nullable();
            $table->string('doctor')->nullable();
            $table->string('recommended_sessions', 50);
            $table->string('observations', 400);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('medical_dx_physio_id')->references('id')->on('hc_cat_medical_dx_physiotherapy');
            $table->foreign('specific_evaluation_pain_id')->references('id')->on('hc_specific_evaluation_pains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_diagnosis_physios');
    }
}
