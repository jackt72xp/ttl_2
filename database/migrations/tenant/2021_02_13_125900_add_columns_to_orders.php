<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('culqi_has_failed')->default(false)->after('purchase');
            $table->text('culqi_response')->nullable()->after('culqi_has_failed');
            $table->unsignedInteger('user_id')->nullable()->after('external_id');

			$table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['culqi_has_failed', 'culqi_response', 'user_id']);
        });
    }
}
