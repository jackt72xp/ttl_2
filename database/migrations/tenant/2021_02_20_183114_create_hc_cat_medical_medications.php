<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateHcCatMedicalMedications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_cat_medical_medications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('hc_cat_medical_medications')->insert([

            ['name' => 'Antiinflamatorios o analgésicos', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Antibióticos', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Relajantes musculares', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Antidepresivos', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Estatinas', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Tamoxifeno', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Anticoagulantes', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Antihistamínicos', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Vitaminas', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Presión Arterial', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Med. Para Tiroides', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Proteínas', 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_cat_medical_medications');
    }
}
