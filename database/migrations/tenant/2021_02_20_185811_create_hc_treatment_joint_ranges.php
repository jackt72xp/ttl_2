<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHcTreatmentJointRanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hc_treatment_joint_ranges', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('treatment_id');
            $table->unsignedInteger('reference_id');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('treatment_id')->references('id')->on('hc_treatments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hc_treatment_joint_ranges');
    }
}
