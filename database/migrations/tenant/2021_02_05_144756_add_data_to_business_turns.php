<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataToBusinessTurns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('business_turns')->insert([
            ['id' => '5', 'value' => 'consulting', 'name' => 'Clinica', 'active' => false, 'created_at'=> now(), 'updated_at'=> now()],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('business_turns')->where('id', 5)->delete();
    }
}
