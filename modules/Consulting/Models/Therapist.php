<?php

namespace Modules\Consulting\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Tenant\ModelTenant;


class Therapist extends ModelTenant
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'last_name',
    ];
}
