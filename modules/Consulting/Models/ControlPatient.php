<?php

namespace Modules\Consulting\Models;

use App\Models\Tenant\Document;
use App\Models\Tenant\Person;
use App\Models\Tenant\ModelTenant;
use Illuminate\Database\Eloquent\SoftDeletes;

class ControlPatient extends ModelTenant
{
    use SoftDeletes;

    protected $fillable = [
        'type_patient',
        'customer_id',
        'appointment_time',
        'appointment_date',
        'appointment_request',
        'service_id',
        'therapist_id',
        'hours',
        'session_current',
        'session_numbe_total',
        'promotion_id',
        'payment_type',
        'document_id',
        'observations',
        'send_email',
    ];

    public function customer() {
        return $this->belongsTo(Person::class, 'customer_id');
    }

    public function document() {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function promotion() {
        return $this->belongsTo(PromotionConsult::class, 'promotion_id');
    }

    public function therapist() {
        return $this->belongsTo(Therapist::class, 'therapist_id');
    }

    public function service() {
        return $this->belongsTo(ServiceConsult::class, 'service_id');
    }

    protected $casts = [
        //'appointment_time' => 'time',
        //'appointment_date' => 'date',

    ];
}
