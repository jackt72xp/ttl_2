<?php

namespace Modules\Consulting\Models;

use App\Models\Tenant\ModelTenant;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceConsult extends ModelTenant
{
    use SoftDeletes;

    protected $fillable = [
        'name',
    ];
}
