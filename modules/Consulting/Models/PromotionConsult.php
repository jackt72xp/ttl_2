<?php

namespace Modules\Consulting\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Tenant\ModelTenant;

class PromotionConsult extends ModelTenant
{
    use SoftDeletes;

    protected $fillable = [
        'name',
    ];
}
