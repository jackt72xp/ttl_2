<?php

namespace Modules\Consulting\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tenant\Person;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Consulting\Models\Therapist;
use Modules\Consulting\Models\ControlPatient;
use Modules\Consulting\Models\ServiceConsult;
use Modules\Consulting\Models\PromotionConsult;
use Modules\Consulting\Http\Requests\ControlPatientRequest;
use Modules\Consulting\Http\Resources\ControlPatientResource;
use Modules\Consulting\Http\Resources\ControlPatientCollection;

class ConsultingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('consulting::index');
    }

    public function records(Request $request) {


        $records = ControlPatient::latest();

        $appointment_request = $request->appointment_request;
        $appointment_date = $request->appointment_date;
        $customer_id = $request->customer_id;
        $service_id = $request->service_id;
        $therapist_id = $request->therapist_id;
        $payment_type = $request->payment_type;
        $reference_document = $request->reference_document;

        if($appointment_request) {
            $records = $records->where('appointment_request', $appointment_request);
        }

        if($appointment_date) {
            $records = $records->where('appointment_date', $appointment_date);
        }

        if($customer_id) {
            $records = $records->where('customer_id', $customer_id);
        }

        if($service_id) {
            $records = $records->where('service_id', $service_id);
        }

        if($therapist_id) {
            $records = $records->where('therapist_id', $therapist_id);
        }

        if($payment_type) {
            $records = $records->where('payment_type', $payment_type);
        }

        if($reference_document == 'not_document') {
            $records = $records->whereNull('document_id');
        }
        else if($reference_document == 'yes_document') {
            $records = $records->whereNotNull('document_id');

        }

        /*if($document_apply){
            $records = $records->where('payment_type', $payment_type);
        }*/

        return new ControlPatientCollection($records->paginate(config('tenant.items_per_page')));
    }

    public function record($id)
    {
        return new ControlPatientResource(ControlPatient::findOrFail($id));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('consulting::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ControlPatientRequest $request)
    {
        $id = $request->input('id');
        $brand = ControlPatient::firstOrNew(['id' => $id]);
        $brand->fill($request->all());
        $brand->save();

        return [
            'success' => true,
            'message' => ($id)?'Registro editado con éxito':'Registrado con éxito',
            'data' => $brand
        ];
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('consulting::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $recordId = $id;
        return view('consulting::edit', compact('recordId'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $bank_account = ControlPatient::findOrFail($id);
            $bank_account->delete();

            return [
                'success' => true,
                'message' => 'Registro eliminado con éxito'
            ];

        } catch (\Exception $e) {

            return ($e->getCode() == '23000') ? ['success' => false,'message' => 'El registro esta siendo usada por otros registros, no puede eliminar'] : ['success' => false,'message' => 'Error inesperado, no se pudo eliminar'];

        }
    }

    public function tables(Request $request) {

        $customer_query = Person::whereType('customers')->whereIsEnabled()->orderBy('name');
        if($request->customerId){
            $customer_query = $customer_query->where('id', $request->customerId);
        }
        $customers = $customer_query->take(20)->get()->transform(function($row) {
            return [
                'id' => $row->id,
                'description' => $row->number.' - '.$row->name,
                'name' => $row->name,
                'number' => $row->number,
                'identity_document_type_id' => $row->identity_document_type_id,
                'identity_document_type_code' => $row->identity_document_type->code,
                'addresses' => $row->addresses,
                'address' =>  $row->address,
                'telephone' => $row->telephone,
                'email' => $row->email,
            ];
        });

        $services = ServiceConsult::select('id', 'name')->get();
        $therapists = Therapist::select('id', 'name', 'last_name')->get();
        $promotions = PromotionConsult::select('id', 'name')->get();

        return compact('customers', 'services', 'therapists', 'promotions');
    }

    public function searchCustomers(Request $request)
    {

        $customers = Person::where('number','like', "%{$request->input}%")
                            ->orWhere('name','like', "%{$request->input}%")
                            ->whereType('customers')->orderBy('name')
                            ->whereIsEnabled()
                            ->get()->transform(function($row) {
                                return [
                                    'id' => $row->id,
                                    'description' => $row->number.' - '.$row->name,
                                    'name' => $row->name,
                                    'number' => $row->number,
                                    'identity_document_type_id' => $row->identity_document_type_id,
                                    'identity_document_type_code' => $row->identity_document_type->code,
                                    'addresses' => $row->addresses,
                                    'address' =>  $row->address,
                                    'telephone' => $row->telephone,
                                    'email' => $row->email,

                                ];
                            });

        return compact('customers');
    }

    public function searchCustomerById($id)
    {
        $customers = Person::whereType('customers')
                    ->where('id',$id)
                    ->get()->transform(function($row) {
                        return [
                            'id' => $row->id,
                            'description' => $row->number.' - '.$row->name,
                            'name' => $row->name,
                            'number' => $row->number,
                            'identity_document_type_id' => $row->identity_document_type_id,
                            'identity_document_type_code' => $row->identity_document_type->code,
                            'telephone' => $row->telephone,
                            'email' => $row->email,

                        ];
                    });

        return compact('customers');
    }

    public function referenceDocument(Request $request) {
        $record = ControlPatient::findOrFail($request->id);
        $record->document_id = $request->documentId;
        $record->save();

        return [
            'success' => true,
            'message' => 'Control paciente referenciado con documento ' . $request->documentId
        ];
    }

    public function updateEmail($id) {
        $record = ControlPatient::findOrFail($id);
        $record->send_email = 1;
        $record->save();
        return [
            'success' => true
        ];
    }
}
