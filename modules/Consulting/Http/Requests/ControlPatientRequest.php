<?php

namespace Modules\Consulting\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ControlPatientRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [
            'type_patient' => [
                'required',
            ],
            'customer_id' => [
                'required',
            ],
            'appointment_time' => [
                'required',
            ],
            'appointment_date' => [
                'required',
            ],
            'appointment_request' => [
                'required',
            ],
            'service_id' => [
                'required_if:appointment_request,"REQUERIDA"',
            ],
            'therapist_id' => [
                'required_if:appointment_request,"REQUERIDA"',
            ],
            'hours' => [
                'required_if:appointment_request,"REQUERIDA"',
            ],
            'session_current' => [
                'required_if:appointment_request,"REQUERIDA"',
            ],
            'session_numbe_total' => [
                'required_if:appointment_request,"REQUERIDA"',
            ],
            /*'promotion_id' => [
                'required_if:appointment_request,"REQUERIDA"',
            ],*/
            'payment_type' => [
                'required_if:appointment_request,"REQUERIDA"',
            ],
        ];

    }

    public function attributes()
    {
        return [
            'service_id' => 'servicio adquirido',
            'appointment_request' => 'solicitud de cita',
            'therapist_id' => 'licenciado',
            'hours' => 'horas',
            'promotion_id' => 'promoción',
            'payment_type' => 'tipo de pago',
        ];
    }

    public function messages()
    {
        return [
            'hours.required_if' => 'El campo es obligatorio.',
            'session_current.required_if' => 'El campo es obligatorio.',
            'session_numbe_total.required_if' => 'El campo es obligatorio.',
        ];
    }
}
