<?php

namespace Modules\Consulting\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ControlPatientCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row) {
            return [
                'id' => $row->id,
                'type_patient'=> $row->type_patient,
                'payment_type'=> $row->payment_type,
                'customer_id'=> $row->customer_id,
                'customer'=> $row->customer->name,
                'appointment_date'=> $row->appointment_date,
                'appointment_time'=> $row->appointment_time,
                'appointment_request'=> $row->appointment_request,
                'session_numbe_total'=> $row->session_numbe_total,
                'observations'=> $row->observations,
                'service_id'=> $row->service_id,
                'service'=> $row->service ? "{$row->service->name}" : '',
                'therapist_id'=> $row->therapist_id,
                'therapist'=> $row->therapist ? "{$row->therapist->name} {$row->therapist->last_name}" : '' ,
                'hours'=> $row->hours,
                'session_current'=> $row->session_current,
                'promotion_id'=> $row->promotion_id,
                'promotion'=> $row->promotion ? $row->promotion->name : '',
                'document_id' => $row->document_id,
                'document' => ($row->document)? $row->document->number_full : '',
                'email' => ((bool)$row->send_email)? 'Si' : 'No'

            ];
        });

    }

}
