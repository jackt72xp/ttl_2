<?php

namespace Modules\Consulting\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ControlPatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request) {
        return [
            'id' => $this->id,
            'type_patient'=> $this->type_patient,
            'customer_id'=> $this->customer_id,
            'appointment_date' => $this->appointment_date,
            'appointment_time'=> $this->appointment_time,
            'appointment_request'=> $this->appointment_request,
            'service_id'=> $this->service_id,
            'service'=> $this->service ?  $this->service->name : null,
            'therapist_id'=> $this->therapist_id,
            'hours'=> $this->hours,
            'session_current'=> $this->session_current,
            'promotion_id'=> $this->promotion_id,
            'promotion'=> $this->promotion ? $this->promotion->name : null,
            'session_numbe_total'=> $this->session_numbe_total,
            'payment_type'=> $this->payment_type,
            'document_id'=> $this->document_id,
            'observations'=> $this->observations,
        ];
    }
}
