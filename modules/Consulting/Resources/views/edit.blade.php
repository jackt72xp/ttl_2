@extends('tenant.layouts.app')

@section('content')
    <div class="row">
        <div class="col-xlg-12 col-lg-12 col-md-12">
            <tenant-consulting-edit :record-id="{{ $recordId }}" ></tenant-consulting-edit>
        </div>
    </div>
@endsection
