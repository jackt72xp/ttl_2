@extends('tenant.layouts.app')

@section('content')
    <div class="row">
        <div class="col-xlg-12 col-lg-12 col-md-12">
            <tenant-consulting-create></tenant-consulting-create>
        </div>
    </div>
@endsection
