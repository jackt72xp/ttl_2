<?php

$hostname = app(Hyn\Tenancy\Contracts\CurrentHostname::class);

if($hostname) {
    Route::domain($hostname->fqdn)->group(function () {
        Route::middleware(['auth', 'locked.tenant'])->group(function() {

            Route::prefix('consulting')->group(function () {

                Route::get('tables', 'ConsultingController@tables');
                Route::get('records', 'ConsultingController@records');
                Route::get('record/{id}', 'ConsultingController@record');
                Route::post('validate_hotel', 'ConsultingController@validate_hotel');
                Route::post('', 'ConsultingController@store');
                Route::get('', 'ConsultingController@index')->name('tenant.consulting.index');
                Route::get('create', 'ConsultingController@create')->name('tenant.consulting.create');
                Route::get('edit/{id}', 'ConsultingController@edit');

                Route::get('search/customers', 'ConsultingController@searchCustomers');
                Route::get('search/customer/{id}', 'ConsultingController@searchCustomerById');
                Route::delete('{id}', 'ConsultingController@destroy');
                Route::post('referenceDocument', 'ConsultingController@referenceDocument');

                Route::get('updateEmail/{id}', 'ConsultingController@updateEmail');

            });

        });
    });
}
