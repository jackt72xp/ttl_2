<?php

$hostname = app(Hyn\Tenancy\Contracts\CurrentHostname::class);

if($hostname) {
    Route::domain($hostname->fqdn)->group(function () {
        Route::middleware(['auth', 'locked.tenant'])->group(function() {

            Route::prefix('hc')->group(function () {

                Route::prefix('persons')->group(function () {
                    Route::get('', 'PersonController@index')->name('tenant.hc.person.index');
                    Route::get('create', 'PersonController@create')->name('tenant.hc.person.create');
                    Route::get('edit/{id}', 'PersonController@edit');
                    Route::post('', 'PersonController@store');
                    Route::get('records', 'PersonController@records');
                    Route::get('tables', 'PersonController@tables');
                    Route::get('record/{id}', 'PersonController@record');
                    Route::get('edit/{id}', 'PersonController@edit');
                    Route::get('columns', 'PersonController@columns');
                    Route::get('remoteSearch', 'PersonController@remoteSearch');
                    Route::get('searchPerson', 'PersonController@searchPerson');
                    Route::delete('{person}', 'PersonController@destroy');

                });

            });

        });
    });
}

