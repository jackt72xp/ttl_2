<?php

namespace Modules\Hc\Models;


class EspecificEvaluationPain extends BaseModel
{
    protected $table = "hc_specific_evaluation_pains";

    protected $fillable = [ 'evaluation_eva_id', 'description', 'how_long', 'how_many_times', 'related', 'specific_injury', 'type_pain_niusance', 'evolution_pain_day', 'aggravates_condition',
                            'makes_you_better', 'functional_limitation_avd', 'functional_limitation_work', 'functional_limitation_recreation', 'articular_id', 'body_zone', 'reason'];

    public function eva()
    {
        return $this->belongsTo(EvaluationEva::class);
    }

    public function articulation()
    {
        return $this->belongsTo(MedicalArticulation::class, 'articular_id');
    }

}
