<?php

namespace Modules\Hc\Models;


class MedicalPathology extends BaseModel
{
    const TYPE_GENERAL = 'GENERAL';
    const TYPE_MUSCULO_ARTICULACION = 'MUSCULO_ARTICULACION';
    const TYPE_GASTROINTESTINAL = 'GASTROINTESTINAL';
    const TYPE_CARDIOVASCULAR = 'CARDIOVASCULAR';
    const TYPE_INFECCIONES = 'INFECCIONES';
    const TYPE_RESPIRATORIO = 'RESPIRATORIO';

    protected $table = "cat_persistent_pathologies";

    public function scopeWhereTypeGeneral($query)
    {
        return $query->where('type', self::TYPE_GENERAL);
    }

    public function scopeWhereTypeMuscle($query)
    {
        return $query->where('type', self::TYPE_MUSCULO_ARTICULACION);
    }
}
