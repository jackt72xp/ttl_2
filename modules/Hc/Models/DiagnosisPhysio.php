<?php

namespace Modules\Hc\Models;


class DiagnosisPhysio extends BaseModel
{
    protected $table = "hc_diagnosis_physios";
    protected $fillable = ['clinic_history_id', 'medical_dx_physio_id', 'forecast', 'structure_bio', 'doctor', 'recommended_sessions'];

}
