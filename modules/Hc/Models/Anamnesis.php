<?php

namespace Modules\Hc\Models;

class Anamnesis extends BaseModel
{
    protected $table = "hc_anamneses";

    protected $fillable = [ 'user_id', 'has_previous_physiotherapy', 'name_previous_physiotherapy', 'date_previous_physiotherapy', 'has_chiropractor',
    'has_chiropractor_date', 'has_osteopathy', 'has_osteopathy_date', 'has_acupuncture', 'has_acupuncture_date', 'has_other_description', 'has_other_date', 'has_psychological_diagnosis',
    'psychological_diagnosis_description', 'has_surgery', 'surgery_date', 'surgery_description', 'has_fracture', 'fracture_date', 'fracture_description', 'has_cancer', 'cancer_date', 'cancer_family_direct',
    'cancer_description', 'has_smoke', 'smoke_frecuency', 'has_allergy', 'allergy_description', 'has_physical_activity', 'physical_activity_frecuency', 'physical_activity_type',
    'pathologie_general_other', 'pathologie_muscle_art_other', 'pathologie_gastro_other', 'pathologie_cardio_other', 'pathologie_infect_other', 'pathologie_respiratory_other', 'medications_other'];

    public function pathologies()
    {
        return $this->belongsToMany(MedicalPathology::class, 'anamneses_pathologies');
    }

    public function medications()
    {
        return $this->belongsToMany(MedicalMedication::class, 'anamneses_medications');
    }

}
