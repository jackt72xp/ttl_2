<?php

namespace Modules\Hc\Models;


class GeneralEvaluationTest extends BaseModel
{
    protected $table = "hc_general_evaluation_tests";

    protected $fillable = ['general_evaluation_id', 'type', 'result', 'medical_test_id', 'positive'];
}
