<?php

namespace Modules\Hc\Models;
use App\Casts\Json;

class GeneralEvaluationLimitation extends BaseModel
{
    protected $table = "hc_general_evaluation_limitations";

    protected $fillable = ['general_evaluation_id', 'type', 'articular_id', 'muscle_id', 'result', 'movement', 'observations'];

    public function articulation()
    {
        return $this->belongsTo(MedicalArticulation::class, 'articular_id');
    }

    public function muscle()
    {
        return $this->belongsTo(MedicalMuscle::class, 'muscle_id');
    }

    protected $casts = [
        'movement' => Json::class,
    ];
}
