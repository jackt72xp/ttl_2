<?php

namespace Modules\Hc\Models;

use App\Models\Tenant\Person;


class ClinicHistory extends BaseModel
{

    protected $table = "hc_clinic_histories";

    protected $fillable = ['person_id'];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

}
