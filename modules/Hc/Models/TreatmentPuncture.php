<?php

namespace Modules\Hc\Models;
use App\Casts\Json;
use phpDocumentor\Reflection\Types\Boolean;

class TreatmentPuncture extends BaseModel
{
    protected $table = "hc_treatment_punctures";

    protected $fillable = ['treatment_id', 'muscle_id', 'muscle', 'articulation_id', 'articulation', 'spasm_left', 'spasm_right', 'medium'];

    protected $casts = [
        'muscle' => Json::class,
        'articulation' => Json::class,
        'medium' => 'boolean',


    ];
}
