<?php

namespace Modules\Hc\Models;


class GeneralEvaluation extends BaseModel
{
    protected $table = "hc_general_evaluations";

    protected $fillable = [ 'clinic_history_id', 'antecedent_child', 'pain_started_another_area', 'has_biomechanical_help', 'biomechanical_help_type', 'has_orthotic', 'orthotic_type', 'recovery_objective'];

    public function reasons()
    {
        return $this->hasMany(EspecificEvaluationPain::class);
    }

}
