<?php

namespace Modules\Hc\Models;
use App\Casts\Json;


class Treatment extends BaseModel
{
    protected $table = "hc_treatments";

    protected $fillable = ['clinic_history_id', 'type', 'date', 'physical_agents', 'used_needles', 'how_pain_now', 'observations', 'do_exercises'];

    public function punctures()
    {
        return$this->hasMany(EvaluationTtoPuncture::class);
    }

    public function ranges()
    {
        return$this->hasMany(EvaluationTtoJointRange::class);
    }

    public function therapies()
    {
        return$this->hasMany(EvaluationTtoManualTherapy::class);
    }

    public function exercises()
    {
        return$this->hasMany(EvaluationTtoPrescriptionExercises::class);
    }

    public function advances()
    {
        return$this->hasMany(EvaluationTtoAdvancePain::class);
    }

    protected $casts = [
        'physical_agents' => Json::class,
    ];
}
