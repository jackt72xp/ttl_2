<?php

namespace Modules\Hc\Models;
use App\Casts\Json;

class TreatmentManualTherapy extends BaseModel
{
    protected $table = "hc_treatment_manual_therapies";

    protected $fillable = ['treatment_id', 'therapy', 'description', 'type', 'observations'];

    protected $casts = [
        'therapy' => Json::class,
    ];
}
