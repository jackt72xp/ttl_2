<?php

namespace Modules\Hc\Models;


class PersonInformation extends BaseModel
{
    protected $table = "hc_person_information";

    protected $fillable = [
        'person_id','type_find_ttl', 'accompanied', 'medical_treating', 'occupation', 'observations', 'type_insurance_id', 'name', 'last_name'
    ];

    /*public function getTypeFindTtlAttribute($value)
    {
        return (is_null($value))?null:(object) json_decode($value);
    }

    public function setTypeFindTtlAttribute($value)
    {
        $this->attributes['type_find_ttl'] = (is_null($value))?null:json_encode($value);
    }*/


}
