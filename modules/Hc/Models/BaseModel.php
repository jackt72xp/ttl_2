<?php

namespace Modules\Hc\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hyn\Tenancy\Traits\UsesTenantConnection;


class BaseModel extends Model
{
    use SoftDeletes;
    use UsesTenantConnection;
}
