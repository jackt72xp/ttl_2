<?php

namespace Modules\Hc\Models;


class EvaluationTtoPrescriptionExercises extends BaseModel
{
    protected $table = "hc_treatment_prescription_exercises";

    protected $fillable = ['treatment_id', 'description', 'type'];
}
