<?php

namespace Modules\Hc\Models;


class TreatmentJointRange extends BaseModel
{
    protected $table = "hc_treatment_joint_ranges";

    protected $fillable = ['treatment_id', 'reference_id', 'description'];
}
