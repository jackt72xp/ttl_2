<?php

namespace Modules\Hc\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'identity_document_type_id' => $this->identity_document_type_id,
            'number' => $this->number,
            'name' => $this->name,
            'last_name' => $this->last_name,
            'name' => $this->hc_information ? $this->hc_information->name : null,
            'last_name' => $this->hc_information ? $this->hc_information->last_name : null,
            'internal_code' => $this->internal_code,
            'trade_name' => $this->trade_name,
            'country_id' => $this->country_id,
            'department_id' => $this->department_id,
            'province_id' => $this->province_id,
            'district_id' => $this->district_id,
            'address' => $this->address,
            'telephone' => $this->telephone,
            'email' => $this->email,
            'perception_agent' => (bool) $this->perception_agent,
            'percentage_perception' => $this->percentage_perception,
            'state' => $this->state,
            'condition' => $this->condition,
            'person_type_id' => $this->person_type_id,
            'contact' => $this->contact,
            'comment' => $this->comment,
            'gender' => $this->gender,
            'birthdate' => $this->birthdate,
            'type_find_ttl' => $this->hc_information ? $this->hc_information->type_find_ttl : null,
            'accompanied' => $this->hc_information ? $this->hc_information->accompanied : null,
            'occupation' => $this->hc_information ? $this->hc_information->occupation : null,
            'observations' => $this->hc_information ? $this->hc_information->observations : null,
            'type_insurance_id' => $this->hc_information ? $this->hc_information->type_insurance_id : null,
        ];
    }
}
