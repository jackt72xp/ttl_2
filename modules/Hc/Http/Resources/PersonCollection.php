<?php

namespace Modules\Hc\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PersonCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'number' => $row->number,
                'name' => $row->hc_information ? $row->hc_information->name : null,
                'last_name' => $row->hc_information ? $row->hc_information->last_name : null,
                'telephone' => $row->telephone,
                'gender' => $row->gender,
                'birthdate' => $row->birthdate,
                'email' => $row->email,
                'internal_code' => $row->internal_code,
                'document_type' => $row->identity_document_type->description,
                'enabled' => (bool) $row->enabled,
                'created_at' => $row->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $row->updated_at->format('Y-m-d H:i:s'),
                'district' => $row->district ? $row->district->description : '',
                'hclinic' => $row->hc_history_clinic ? "HC-".str_pad($row->hc_history_clinic->id, 4, "0", STR_PAD_LEFT) : null
            ];
        });
    }
}
