<?php

namespace Modules\Hc\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PersonRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [
            'name' => 'required',
            'last_name' => 'required',
            'email' => ['required', 'email', 'unique:users',  Rule::unique('users')->ignore($id)],
            'gender' => 'required',
            'phone' => 'required',
            'identity_document_type_id' => 'required',
            'number_document' => ['required', 'unique:users', Rule::unique('users')->ignore($id)],
            'birthdate' => 'required|date',
            'occupation' => 'required',
            //'type_insurance_id' => 'required',
            'country_id' => 'required',
            'department_id' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'medical_treating' => 'required',
            'specialty_id' => 'required',
            'diagnosis_id' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'specialty_id' => 'especialidad',
            'diagnosis_id' => 'diagnostico médico',

        ];
    }
}
