<?php

namespace Modules\Hc\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Tenant\Person;
use Modules\Hc\Http\Resources\PersonCollection;
use Modules\Hc\Http\Resources\PersonResource;

use Modules\Hc\Http\Requests\PersonRequest;
use Modules\Hc\Models\PersonInformation;
use Modules\Hc\Models\ClinicHistory;
use Modules\Hc\Models\TypeInsurance;
use Modules\Hc\Models\MedicalSpecialty;
use Modules\Hc\Models\MedicalDiagnostic;
use App\Models\Tenant\Catalogs\Department;
use App\Models\Tenant\Catalogs\District;
use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Models\Tenant\Catalogs\Province;


class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hc::person.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hc::person.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');
        $person = Person::firstOrNew(['id' => $id]);
        $person->fill($request->all());
        $person->name = $request->name.''.$request->last_name;
        $person->save();

        $person_information = PersonInformation::firstOrNew(['person_id' => $id]);
        $person_information->fill($request->all());
        $person_information->person_id = $person->id;
        $person_information->save();

        $person->diagnostics()->sync($request->diagnosis_id);
        $person->specialties()->sync($request->specialty_id);

        $person->save();

        if(!$id)
        {
            $history = new ClinicHistory;
            $history->person_id = $person->id;
            $history->save();
        }

        return [
            'success' => true,
            'data'=> ['id' => $person->id],
            'message' => ($id)?'Registro editado con éxito':'Registrado con éxito'
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('hc::person.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $person = Person::findOrFail($id);
            $person_type = ($person->type == 'customers') ? 'Cliente':'Proveedor';
            $person->delete();

            return [
                'success' => true,
                'message' => $person_type.' eliminado con éxito'
            ];

        } catch (\Exception $e) {

            return ($e->getCode() == '23000') ? ['success' => false,'message' => "El {$person_type} esta siendo usado por otros registros, no puede eliminar"] : ['success' => false,'message' => "Error inesperado, no se pudo eliminar el {$person_type}"];
        }
    }

    public function records(Request $request)
    {
        $records = Person::where('type', 'customers')->orderBy('name');

        $identity_document_type_id = $request->identity_document_type_id;
        $number = $request->number;
        $column = $request->column;
        $value = $request->value;

        if ($identity_document_type_id && $number) {
            $records = $records->where([
                ['identity_document_type_id', $identity_document_type_id],
                ['number', $number],
            ]);
        }

        if ($column && $value){
            $records = $records->where($column, 'like', "%{$value}%");
        }

        return new PersonCollection($records->paginate(config('tenant.items_per_page')));
    }

    public function record($id) {
        $record = new PersonResource(Person::findOrFail($id));
        return $record;
    }

    public function tables () {

        $departments = Department::whereActive()->orderByDescription()->get();
        //$provinces = Province::whereActive()->orderByDescription()->get();
        //$districts = District::whereActive()->orderByDescription()->get();
        $identity_document_types = IdentityDocumentType::whereActive()->get();
        $insurance_types = TypeInsurance::all();
        //$medical_specialties = MedicalSpecialty::select('id', 'name')->get();
        //$medical_diagnostics = MedicalDiagnostic::select('id', 'name')->limit(5)->get();

        return compact('departments', 'identity_document_types', 'insurance_types'); //, 'medical_specialties', 'medical_diagnostics');
    }

    public function columns()
    {
        return [
            'name' => 'Nombre',
            'number' => 'Número',
            'document_type' => 'Tipo de documento'
        ];
    }

    public function remoteSearch(Request $request) {
        if($request->table == 'medical_diagnostics')
        {
            $search = strtoupper($request->search);
            $rows = MedicalDiagnostic::select('id', 'name')->where('name', 'like', "%{$search}%");
            if($request->ids)
            {
                $rows = $rows->orWhereIn('id', $request->ids);
            }
            return [
                'data' => $rows->limit(5)->get()
            ];
        }

        if($request->table == 'provinces')
        {
            return [
                'data' => Province::select('id', 'description')->where('department_id', $request->department_id)->get()
            ];
        }

        if($request->table == 'districts')
        {
            return [
                'data' => District::select('id', 'description')->where('province_id', $request->province_id)->where('description', 'like', "%{$request->search}%")->get()
            ];
        }

    }

    public function searchPerson(Request $request) {
        $identity_document_type_id = $request->identity_document_type_id;
        $number = $request->number;
        $record = Person::where('type', 'customers')->where('identity_document_type_id', $identity_document_type_id)->where('number', trim($number))->first();
        return [
            'success' => true,
            'data' => $record,
        ];
    }
}
